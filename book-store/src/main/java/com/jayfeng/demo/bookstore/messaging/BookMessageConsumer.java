package com.jayfeng.demo.bookstore.messaging;

import com.jayfeng.demo.bookstore.model.Author;
import com.jayfeng.demo.bookstore.model.Book;
import com.jayfeng.demo.bookstore.model.BookMessage;
import com.jayfeng.demo.bookstore.repository.AuthorRepository;
import com.jayfeng.demo.bookstore.repository.BookAuthorRepository;
import com.jayfeng.demo.bookstore.repository.BookRepository;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.cloud.stream.annotation.StreamListener;
import org.springframework.stereotype.Component;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Map;
import java.util.concurrent.CompletableFuture;
import java.util.stream.Collectors;

@Component
public class BookMessageConsumer {

  private static final Logger LOG = LogManager.getLogger();

  private BookRepository bookRepository;
  private AuthorRepository authorRepository;
  private BookAuthorRepository bookAuthorRepository;

  public BookMessageConsumer(BookRepository bookRepository,
                             AuthorRepository authorRepository,
                             BookAuthorRepository bookAuthorRepository) {
    this.bookRepository = bookRepository;
    this.authorRepository = authorRepository;
    this.bookAuthorRepository = bookAuthorRepository;
  }

  @StreamListener(Channels.BOOKS_CHANNEL)
  public void consumeBookMessages(Collection<BookMessage> bookMessages) {
    LOG.info("Received {} book messages", bookMessages.size());
    this.processBookMessages(bookMessages);
  }

  private void processBookMessages(Collection<BookMessage> bookMessages) {
    Map<Book, Collection<Author>> bookAuthorMap = bookMessages.stream()
        .collect(Collectors.toConcurrentMap(this::extractBook, this::extractAuthors, this::mergeAuthors));

    CompletableFuture<Void> persistBooksFuture = CompletableFuture.runAsync(() -> {
      LOG.info("Persisting {} books", bookAuthorMap.keySet().size());
      bookRepository.saveAll(bookAuthorMap.keySet());
    });

    CompletableFuture<Void> persistAuthorsFuture = CompletableFuture.runAsync(() -> {
      Collection<Author> flattenedAuthors = new ArrayList<>();
      for (Collection<Author> authors : bookAuthorMap.values()) {
        flattenedAuthors.addAll(authors);
      }

      LOG.info("Persisting {} Authors", flattenedAuthors.size());
      authorRepository.saveAll(flattenedAuthors);
    });

    CompletableFuture.allOf(persistBooksFuture, persistAuthorsFuture)
        .thenRunAsync(() -> bookAuthorMap.forEach(bookAuthorRepository::linkBookAndAuthors))
        .exceptionally(e -> {
          LOG.error(e.getLocalizedMessage(), e);
          return null;
        });
  }

  private Book extractBook(BookMessage bookMessage) {
    LocalDate publishedDate = bookMessage.getPublishedDate() == null ? null : bookMessage.getPublishedDate().toLocalDate();
    return new Book(bookMessage.getIsbn(), bookMessage.getTitle(), bookMessage.getThumbnailUrl(), bookMessage.getPageCount(), publishedDate);
  }

  private Collection<Author> extractAuthors(BookMessage bookMessage) {
    return bookMessage.getAuthors().stream().map(Author::new).collect(Collectors.toList());
  }

  private Collection<Author> mergeAuthors(Collection<Author> a, Collection<Author> b) {
    a.addAll(b);
    return a;
  }
}
