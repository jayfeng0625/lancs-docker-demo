package com.jayfeng.demo.bookstore.repository;

import com.jayfeng.demo.bookstore.model.Book;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.Collection;

@Mapper
@Repository
public interface BookRepository {

  Collection<Book> findAll();

  Book findById(@Param("id") String id);

  Book findByIsbn(@Param("isbn") String isbn);

  Collection<Book> findByQuery(@Param("title") String title);

  Collection<Book> findByAuthor(@Param("author") String author);

  void saveAll(@Param("books") Collection<Book> books);
}
