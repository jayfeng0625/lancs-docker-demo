package com.jayfeng.demo.bookstore.model;

import java.time.LocalDate;
import java.util.Objects;

public class Book {

  private String id;
  private String isbn;
  private String title;
  private String thumbnailUrl;
  private Integer pageCount;
  private LocalDate publishedDate;

  public Book(String isbn, String title, String thumbnailUrl, Integer pageCount, LocalDate publishedDate) {
    this.isbn = isbn;
    this.title = title;
    this.thumbnailUrl = thumbnailUrl;
    this.pageCount = pageCount;
    this.publishedDate = publishedDate;
  }

  public Book(String id, String isbn, String title, String thumbnailUrl, Integer pageCount, LocalDate publishedDate) {
    this.id = id;
    this.isbn = isbn;
    this.title = title;
    this.thumbnailUrl = thumbnailUrl;
    this.pageCount = pageCount;
    this.publishedDate = publishedDate;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;
    Book book = (Book) o;
    return Objects.equals(id, book.id) &&
        Objects.equals(isbn, book.isbn) &&
        Objects.equals(title, book.title);
  }

  @Override
  public int hashCode() {
    return Objects.hash(id, isbn, title);
  }

  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public String getIsbn() {
    return isbn;
  }

  public void setIsbn(String isbn) {
    this.isbn = isbn;
  }

  public String getTitle() {
    return title;
  }

  public void setTitle(String title) {
    this.title = title;
  }

  public String getThumbnailUrl() {
    return thumbnailUrl;
  }

  public void setThumbnailUrl(String thumbnailUrl) {
    this.thumbnailUrl = thumbnailUrl;
  }

  public Integer getPageCount() {
    return pageCount;
  }

  public void setPageCount(Integer pageCount) {
    this.pageCount = pageCount;
  }

  public LocalDate getPublishedDate() {
    return publishedDate;
  }

  public void setPublishedDate(LocalDate publishedDate) {
    this.publishedDate = publishedDate;
  }
}
