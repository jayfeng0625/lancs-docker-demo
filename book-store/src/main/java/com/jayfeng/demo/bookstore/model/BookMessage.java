package com.jayfeng.demo.bookstore.model;

import com.fasterxml.jackson.annotation.JsonFormat;

import java.time.ZonedDateTime;
import java.util.Collection;
import java.util.Objects;

public class BookMessage {

  private String isbn;
  private String title;
  private String thumbnailUrl;
  private Collection<String> authors;
  @JsonFormat(pattern = "yyyy-MM-dd'T'HH:mm:ss.SSSZ")
  private ZonedDateTime publishedDate;
  private Integer pageCount;

  public BookMessage() {}

  public String getIsbn() {
    return isbn;
  }

  public void setIsbn(String isbn) {
    this.isbn = isbn;
  }

  public String getTitle() {
    return title;
  }

  public void setTitle(String title) {
    this.title = title;
  }

  public String getThumbnailUrl() {
    return thumbnailUrl;
  }

  public void setThumbnailUrl(String thumbnailUrl) {
    this.thumbnailUrl = thumbnailUrl;
  }

  public Collection<String> getAuthors() {
    return authors;
  }

  public void setAuthors(Collection<String> authors) {
    this.authors = authors;
  }

  public ZonedDateTime getPublishedDate() {
    return publishedDate;
  }

  public void setPublishedDate(ZonedDateTime publishedDate) {
    this.publishedDate = publishedDate;
  }

  public Integer getPageCount() {
    return pageCount;
  }

  public void setPageCount(Integer pageCount) {
    this.pageCount = pageCount;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;
    BookMessage that = (BookMessage) o;
    return Objects.equals(isbn, that.isbn) &&
      Objects.equals(title, that.title) &&
      Objects.equals(authors, that.authors) &&
      Objects.equals(publishedDate, that.publishedDate) &&
      Objects.equals(pageCount, that.pageCount);
  }

  @Override
  public int hashCode() {
    return Objects.hash(isbn, title, authors, publishedDate, pageCount);
  }
}
