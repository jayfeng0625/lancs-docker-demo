package com.jayfeng.demo.bookstore.repository;

import com.jayfeng.demo.bookstore.model.Author;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.Collection;

@Mapper
@Repository
public interface AuthorRepository {

  Collection<Author> findAll();

  void saveAll(@Param("authors") Collection<Author> authors);
}
