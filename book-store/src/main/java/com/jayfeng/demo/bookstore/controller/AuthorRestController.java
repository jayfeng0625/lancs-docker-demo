package com.jayfeng.demo.bookstore.controller;

import com.jayfeng.demo.bookstore.model.Author;
import com.jayfeng.demo.bookstore.repository.AuthorRepository;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Collection;
import java.util.concurrent.CompletableFuture;

@RestController
@RequestMapping("/api/authors")
public class AuthorRestController {

  private AuthorRepository authorRepository;

  public AuthorRestController(AuthorRepository authorRepository) {
    this.authorRepository = authorRepository;
  }

  @GetMapping
  public CompletableFuture<Collection<Author>> getAuthors() {
    return CompletableFuture.supplyAsync(() -> authorRepository.findAll());
  }
}
