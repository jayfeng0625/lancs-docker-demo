package com.jayfeng.demo.bookstore.messaging;

import org.springframework.cloud.stream.annotation.Input;
import org.springframework.messaging.SubscribableChannel;

public interface BookMessageConsumerBinder {

  @Input(Channels.BOOKS_CHANNEL)
  SubscribableChannel booksChannel();

}
