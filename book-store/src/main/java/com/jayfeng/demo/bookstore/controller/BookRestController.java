package com.jayfeng.demo.bookstore.controller;

import com.jayfeng.demo.bookstore.BadRequestException;
import com.jayfeng.demo.bookstore.model.Book;
import com.jayfeng.demo.bookstore.repository.BookRepository;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Collection;
import java.util.concurrent.CompletableFuture;

@RestController
@RequestMapping("/api/books")
public class BookRestController {

  private BookRepository bookRepository;

  public BookRestController(BookRepository bookRepository) {
    this.bookRepository = bookRepository;
  }

  @GetMapping
  public CompletableFuture<Collection<Book>> getBooks() {
    return CompletableFuture.supplyAsync(() -> bookRepository.findAll());
  }

  @GetMapping("/{id}")
  public CompletableFuture<Book> findBookById(@PathVariable("id") String id) {
    return CompletableFuture.supplyAsync(() -> bookRepository.findById(id));
  }

  @GetMapping("/isbn/{isbn}")
  public CompletableFuture<Book> findBookByIsbn(@PathVariable("isbn") String isbn) {
    return CompletableFuture.supplyAsync(() -> bookRepository.findByIsbn(isbn));
  }

  @GetMapping("/search")
  public CompletableFuture<Collection<Book>> findBookByQuery(@RequestParam(value = "title", required = false) String title,
                                                             @RequestParam(value = "author", required = false) String author) {
    if (title != null) {
      if (title.isBlank()) {
        return CompletableFuture.failedFuture(new BadRequestException("Title query invalid"));
      }
      return CompletableFuture.supplyAsync(() -> bookRepository.findByQuery(title));
    }

    if (author != null) {
      if (author.isBlank()) {
        return CompletableFuture.failedFuture(new BadRequestException("Author name query invalid"));
      }
      return CompletableFuture.supplyAsync(() -> bookRepository.findByAuthor(author));
    }
    return getBooks();
  }
}
