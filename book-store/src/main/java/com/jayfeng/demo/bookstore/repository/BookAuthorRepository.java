package com.jayfeng.demo.bookstore.repository;

import com.jayfeng.demo.bookstore.model.Author;
import com.jayfeng.demo.bookstore.model.Book;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.Collection;

@Mapper
@Repository
public interface BookAuthorRepository {

  void linkBookAndAuthors(@Param("book") Book book, @Param("authors") Collection<Author> authors);
}
