package com.jayfeng.demo.bookstore;

import com.jayfeng.demo.bookstore.messaging.BookMessageConsumerBinder;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.stream.annotation.EnableBinding;

@EnableBinding(BookMessageConsumerBinder.class)
@SpringBootApplication
public class BookStoreApplication {

  public static void main(String[] args) {
    SpringApplication.run(BookStoreApplication.class, args);
  }
}
