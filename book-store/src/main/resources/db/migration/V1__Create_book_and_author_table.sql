CREATE EXTENSION IF NOT EXISTS "uuid-ossp";

CREATE TABLE book
(
    id             UUID NOT NULL PRIMARY KEY DEFAULT uuid_generate_v4(),
    isbn           TEXT NOT NULL UNIQUE,
    title          TEXT NOT NULL,
    thumbnail_url  TEXT NULL,
    page_count     INT,
    published_date DATE NULL
);

CREATE TABLE author
(
    id   UUID NOT NULL PRIMARY KEY DEFAULT uuid_generate_v4(),
    name TEXT NOT NULL UNIQUE
);

CREATE TABLE book_author
(
    book_id   UUID NOT NULL REFERENCES book (id),
    author_id UUID NOT NULL REFERENCES author (id),
    CONSTRAINT unique_book_author_id UNIQUE (book_id, author_id)
)
