package com.jayfeng.demo.bookpublisher.messaging;

import org.springframework.cloud.stream.annotation.Output;
import org.springframework.messaging.MessageChannel;

public interface BookMessagePublisherBinder {

  @Output(Channels.BOOKS_CHANNEL)
  MessageChannel output();
}
