package com.jayfeng.demo.bookpublisher.messaging;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.jayfeng.demo.bookpublisher.model.BookMessage;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.core.io.ClassPathResource;
import org.springframework.messaging.MessageChannel;
import org.springframework.messaging.support.GenericMessage;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Random;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;

@Component
public class BookMessagePublisher {

  private static final Logger LOG = LogManager.getLogger();

  private MessageChannel bookMessageChannel;
  private ObjectMapper objectMapper;
  private List<BookMessage> messagesToStream = new ArrayList<>();
  private AtomicInteger index = new AtomicInteger();

  public BookMessagePublisher(BookMessagePublisherBinder binder,
                              ObjectMapper objectMapper) {
    this.bookMessageChannel = binder.output();
    this.objectMapper = objectMapper;
  }

  @PostConstruct
  private void initBookMessages() throws IOException {
    messagesToStream = objectMapper.readerFor(BookMessage.class)
        .<BookMessage>readValues(new ClassPathResource("books.json").getInputStream())
        .readAll();
  }

  @Scheduled(initialDelay = 3000, fixedRate = 1000)
  public void publish() {
    Collection<BookMessage> bookMessages = produceBookMessages();
    if (bookMessages.isEmpty()) {
      return;
    }

    Collection<BookMessage> toPublish = bookMessages.stream()
        .filter(this::isMessageValid)
        .collect(Collectors.toList());

    bookMessageChannel.send(new GenericMessage<>(toPublish));
    LOG.info("Published {} BookMessage objects", toPublish.size());
  }

  private Collection<BookMessage> produceBookMessages() {
    int current = index.get();

    if (current >= messagesToStream.size()) {
      return Collections.emptyList();
    }

    int amount = new Random().nextInt(100) + 1;
    int toIndex = index.addAndGet(amount);

    if (toIndex >= messagesToStream.size()) {
      toIndex = messagesToStream.size();
    }

    return messagesToStream.subList(current, toIndex);
  }

  private boolean isMessageValid(BookMessage message) throws RuntimeException {
    return message.getIsbn() != null && !message.getIsbn().isBlank() &&
        message.getTitle() != null && !message.getTitle().isBlank() &&
        message.getAuthors() != null && !message.getAuthors().isEmpty();
  }
}
