package com.jayfeng.demo.bookpublisher.controller;

import com.jayfeng.demo.bookpublisher.BadRequestException;
import com.jayfeng.demo.bookpublisher.messaging.BookMessagePublisherBinder;
import com.jayfeng.demo.bookpublisher.model.BookMessage;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.messaging.MessageChannel;
import org.springframework.messaging.support.GenericMessage;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Collection;
import java.util.Collections;
import java.util.concurrent.CompletableFuture;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api/books")
public class BookPublisherRestController {

  private static final Logger LOG = LogManager.getLogger();

  private MessageChannel messageChannel;

  public BookPublisherRestController(BookMessagePublisherBinder binder) {
    this.messageChannel = binder.output();
  }

  @PostMapping("/publish")
  public CompletableFuture<Void> publish(@RequestBody BookMessage payload) {
    return CompletableFuture.supplyAsync(() -> payload)
        .thenAccept(bookMessage -> {
          if (isPayloadValid(bookMessage)) {
            messageChannel.send(new GenericMessage<>(Collections.singletonList(bookMessage)));
          } else {
            throw new BadRequestException("Invalid payload");
          }
        })
        .exceptionally(e -> {
          throw new RuntimeException(e);
        });
  }

  @PostMapping("/publish-many")
  public CompletableFuture<Void> publish(@RequestBody Collection<BookMessage> payloads) {
    return CompletableFuture.supplyAsync(() -> payloads)
        .thenApply(bookMessages -> bookMessages.stream().filter(this::isPayloadValid).collect(Collectors.toList()))
        .thenAccept(validatedMessages -> messageChannel.send(new GenericMessage<>(validatedMessages)));
  }

  private boolean isPayloadValid(BookMessage payload) throws RuntimeException {
    if (payload.getIsbn() == null || payload.getIsbn().isBlank() ||
        payload.getTitle() == null || payload.getTitle().isBlank() ||
        payload.getAuthors() == null || payload.getAuthors().isEmpty()) {
      LOG.info("Ignoring invalid payload: {}", payload);
      return false;
    }
    return true;
  }
}
