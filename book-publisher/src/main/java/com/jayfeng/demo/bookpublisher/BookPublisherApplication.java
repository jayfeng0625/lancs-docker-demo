package com.jayfeng.demo.bookpublisher;

import com.jayfeng.demo.bookpublisher.messaging.BookMessagePublisherBinder;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.stream.annotation.EnableBinding;
import org.springframework.scheduling.annotation.EnableScheduling;

@EnableBinding(BookMessagePublisherBinder.class)
@EnableScheduling
@SpringBootApplication
public class BookPublisherApplication {

  public static void main(String[] args) {
    SpringApplication.run(BookPublisherApplication.class, args);
  }
}
